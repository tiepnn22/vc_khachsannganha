<?php

	$get_category =  get_the_category(get_the_id());

	foreach ( $get_category as $get_category_kq ) {
	    $id_category = $get_category_kq->term_id;
	}

	$name_category = get_cat_name($id_category);

	$banner_img_check = get_field('image_cat','category_'.$id_category.'');

	$data = [
	    'id_category' => $id_category,
	    'name_category' => $name_category,
	    'banner_img_check' => $banner_img_check
	];

	view('single', $data);

?>
