<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style() {
	wp_enqueue_style(
		'template-style',
		asset('app.css'),
		false
	);
}

function theme_enqueue_scripts() {
	wp_enqueue_script(
		'template-scripts',
		asset('app.js'),
		'jquery',
		'1.0',
		true
	);

	$protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

	$params = array(
		'ajax_url' => admin_url('admin-ajax.php', $protocol),
	);
	wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
	/**
	 * setup support for theme
	 *
	 * @return void
	 */
	function themeSetup() {
		// Register menus
		register_nav_menus(array(
			'main-menu' => __('Main Menu', 'nganha'),
		));

		// add_theme_support('menus');
		add_theme_support('post-thumbnails');
		add_image_size('album', 270, 270, true);
		add_image_size('room', 370, 250, true);
		add_image_size('news', 185, 140, true);
		add_image_size('news-sidebar', 90, 90, true);
		add_image_size('single-room-detail', 150, 100, true);
	}

	add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
	/**
	 * register sidebar for theme
	 *
	 * @return void
	 */
	function themeSidebars() {
		$sidebars = [
			[
				'name' => __('Sidebar trang tin tức', 'nganha'),
				'id' => 'sidebar-page-news',
				'description' => __('Sidebar trang tin tức', 'nganha'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h2 class="title-widget">',
				'after_title' => '</h2>',
			],
			[
				'name' => __('Sidebar danh mục', 'nganha'),
				'id' => 'sidebar-category',
				'description' => __('Sidebar danh mục', 'nganha'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h2 class="title-widget">',
				'after_title' => '</h2>',
			],
		];

		foreach ($sidebars as $sidebar) {
			register_sidebar($sidebar);
		}
	}

	add_action('widgets_init', 'themeSidebars');
}

if (!function_exists('registerCustomizeFields')) {
	function registerCustomizeFields() {
		$data = [
			[
				'info' => [
					'name' => 'header_top',
					'label' => 'Header top',
					'description' => '',
					'priority' => 1,
				],
				'fields' => [
					[
						'name' => 'tel',
						'type' => 'text',
						'default' => '',
						'label' => 'Tel',
					],
					[
						'name' => 'tel_phone',
						'type' => 'text',
						'default' => '',
						'label' => 'Tel phone',
					],
					[
						'name' => 'socical_facebook',
						'type' => 'text',
						'default' => '',
						'label' => 'Link facebook',
					],
					[
						'name' => 'socical_google',
						'type' => 'text',
						'default' => '',
						'label' => 'Link google',
					],
					[
						'name' => 'socical_twitter',
						'type' => 'text',
						'default' => '',
						'label' => 'Link twitter',
					],
					[
						'name' => 'socical_youtobe',
						'type' => 'text',
						'default' => '',
						'label' => 'Link youtobe',
					],
					[
						'name' => 'socical_tripadviso',
						'type' => 'text',
						'default' => '',
						'label' => 'Link tripadviso',
					],
				],
			],
			[
				'info' => [
					'name' => 'header_bottom',
					'label' => 'Header bottom',
					'description' => '',
					'priority' => 2,
				],
				'fields' => [
					[
						'name' => 'logo',
						'type' => 'upload',
						'default' => '',
						'label' => 'Logo',
					],
					[
						'name' => 'slogan_small',
						'type' => 'text',
						'default' => '',
						'label' => 'Slogan small',
					],
					[
						'name' => 'slogan_large',
						'type' => 'text',
						'default' => '',
						'label' => 'Slogan large',
					],
					[
						'name' => 'image_right',
						'type' => 'upload',
						'default' => '',
						'label' => 'Image right',
					],
				],
			],
			[
				'info' => [
					'name' => 'footer',
					'label' => 'Footer',
					'description' => '',
					'priority' => 3,
				],
				'fields' => [
					[
						'name' => 'address',
						'type' => 'textarea',
						'default' => '',
						'label' => 'Address',
					],
					[
						'name' => 'news',
						'type' => 'select',
						'default' => '',
						'label' => 'Chọn danh mục',
						'choices' => getCategories('category'),
					],
					[
						'name' => 'copyright',
						'type' => 'text',
						'default' => '',
						'label' => 'Copyright',
					],
				],
			],
			[
				'info' => [
					'name' => 'banner',
					'label' => 'Banner trang trong',
					'description' => '',
					'priority' => 4,
				],
				'fields' => [
					[
						'name' => 'no_home',
						'type' => 'upload',
						'default' => '',
						'label' => 'Banner',
					],
				],
			],
		];

		$customizer = new MSC\Customizer\Customizer($data);
		$customizer->create();
	}

	add_action('init', 'registerCustomizeFields');
}

function getCategories($tax) {
	global $wpdb;

	$sql = "SELECT * FROM {$wpdb->prefix}terms as terms
            JOIN {$wpdb->prefix}term_taxonomy as term_tax ON terms.term_id = term_tax.term_id
            WHERE term_tax.taxonomy = '{$tax}'";

	$results = $wpdb->get_results($sql);

	$cates = ['' => ''];

	foreach ($results as $term) {

		$cates[$term->term_id] = $term->name;

	}

	// var_dump($cates);exit;

	return $cates;
}

function getNameCategories($tax) {
	global $wpdb;

	$sql = "SELECT * FROM {$wpdb->prefix}terms as terms
            JOIN {$wpdb->prefix}term_taxonomy as term_tax ON terms.term_id = term_tax.term_id
            WHERE term_tax.taxonomy = '{$tax}'";

	$results = $wpdb->get_results($sql);

	$cates = [];

	foreach ($results as $term) {

		$cates[$term->name] = $term->name;

	}

	// var_dump($cates);exit;

	return $cates;
}

function getNamePostRoom($name) {

	$namepostroom = [];

	$query =  new WP_Query( array(
            'post_type' => $name,
			'posts_per_page' => -1,
    ) );

	while ($query->have_posts() ) : $query->the_post();

		$title_room = get_the_title();
		
		$namepostroom[$title_room] = $title_room;

	endwhile;
    
    // var_dump($namepostroom);exit;

	return $namepostroom;
	
}