<?php

$home_room_type_title = get_field('home_room_type_title');
$home_room_type = get_field('home_room_type');
$home_introduction = get_field('home_introduction');
$home_introduction_bg = get_field('home_introduction_bg');
$home_video = get_field('home_video');

$data = [
    
    'home_room_type_title' => $home_room_type_title,
    'home_room_type' => $home_room_type,
    'home_introduction' => $home_introduction,
    'home_introduction_bg' => $home_introduction_bg,
    'home_video' => $home_video,
];

view('front-page', $data);

?>
