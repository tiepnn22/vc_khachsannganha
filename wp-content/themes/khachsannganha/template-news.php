<?php
/**
 * Template Name: News Template
 * 
 */
	$category_page_news = get_field('category_page_news');
	
	$pageId = get_the_ID();
	$banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
	$banner_img_check = $banner_img[0];

	$name_page = get_the_title();


	$data = [
	    'name_page' => $name_page,
	    'banner_img_check' => $banner_img_check,
	    'category_page_news' => $category_page_news
	];


	view('template-news', $data);

?>