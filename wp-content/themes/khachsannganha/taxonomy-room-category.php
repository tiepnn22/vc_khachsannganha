<?php

	$term_tax = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_tax_id = $term_tax->term_id;
	$term_tax_name = $term_tax->name;

	$banner_img_check = get_field('image_cat','category_'.$term_tax_id.'');

	$data = [
	    'term_tax_id' => $term_tax_id,
	    'term_tax_name' => $term_tax_name,
	    'banner_img_check' => $banner_img_check
	];

	view('room-category', $data);

?>


