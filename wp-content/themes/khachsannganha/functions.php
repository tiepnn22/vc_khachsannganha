<?php
global $wpdb;
define('PREFIX_TABLE', $wpdb->prefix);

$app = require_once __DIR__ . '/bootstrap/app.php';

add_action('pre_get_posts', function ($query) {
	if ($query->is_search) {
		$query->set('posts_per_page', 5);
	}
});

use Vicoders\ContactForm\Abstracts\Input;
use Vicoders\ContactForm\Abstracts\Type;
use Vicoders\ContactForm\Facades\ContactFormManager;

ContactFormManager::add([
	'name' => 'form-contact',
	'type' => Type::CONTACT,
	'style' => 'form-contact',
	'status' => [
		[
			'id' => 1,
			'name' => 'pending',
			'is_default' => true,
		],
		[
			'id' => 2,
			'name' => 'confirmed',
			'is_default' => false,
		],
		[
			'id' => 3,
			'name' => 'cancel',
			'is_default' => false,
		],
		[
			'id' => 4,
			'name' => 'complete',
			'is_default' => false,
		],
	],
	'fields' => [
		[
			'label' => 'Họ tên',
			'name' => 'name', // the key of option
			'type' => Input::TEXT,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Email',
			'name' => 'email', // the key of option
			'type' => Input::EMAIL,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Số điện thoại',
			'name' => 'phone',
			'type' => Input::NUMBER,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Địa chỉ',
			'name' => 'address', // the key of option
			'type' => Input::TEXT,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Nội dung',
			'name' => 'message',
			'type' => Input::TEXTAREA,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'value' => 'Gửi',
			'type' => Input::SUBMIT,
			'attributes' => [
				'class' => 'btn btn-primary btn-submit',
				'placeholder' => __('Gửi', 'contactmodule'),
			],
		],
	],
]);

ContactFormManager::add([
	'name' => 'form-footer',
	'type' => Type::CONTACT,
	'style' => 'form-footer',
	'status' => [
		[
			'id' => 1,
			'name' => 'pending',
			'is_default' => true,
		],
		[
			'id' => 2,
			'name' => 'contacted',
			'is_default' => false,
		],
		[
			'id' => 3,
			'name' => 'cancel',
			'is_default' => false,
		],
	],
	'fields' => [
		[
			'label' => '',
			'name' => 'name', // the key of option
			'type' => Input::TEXT,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => 'Họ tên',
			],
		],
		[
			'label' => '',
			'name' => 'email', // the key of option
			'type' => Input::EMAIL,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => 'Email',
			],
		],
		[
			'label' => '',
			'name' => 'message',
			'type' => Input::TEXTAREA,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => 'Nội dung',
				'cols' => 10,
				'rows' => 5,
			],
		],
		[
			'value' => 'Gửi',
			'type' => Input::SUBMIT,
			'attributes' => [
				'class' => 'btn btn-primary btn-submit',
			],
		],
	],
]);

ContactFormManager::add([
	'name' => 'form-sidebar',
	'type' => Type::CONTACT,
	'style' => 'form-sidebar',
	'status' => [
		[
			'id' => 1,
			'name' => 'pending',
			'is_default' => true,
		],
		[
			'id' => 2,
			'name' => 'publish',
			'is_default' => false,
		],
		[
			'id' => 3,
			'name' => 'cancel',
			'is_default' => false,
		],
		[
			'id' => 4,
			'name' => 'complete',
			'is_default' => false,
		],
	],
	'fields' => [
		[
			'label' => 'Họ tên',
			'name' => 'name', // the key of option
			'type' => Input::TEXT,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Địa chỉ',
			'name' => 'address', // the key of option
			'type' => Input::TEXT,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Email',
			'name' => 'email', // the key of option
			'type' => Input::EMAIL,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Số điện thoại',
			'name' => 'phone',
			'type' => Input::NUMBER,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Loại phòng',
			'name' => 'room_type',
			'type' => Input::SELECT,
			'list' => getNamePostRoom('room'),
			// 'selected' => 'Phòng Delux',
			'selectAttributes' => [
				'class' => 'form-control',
			],
			'optionsAttributes' => [
				'placeholder' => '',
				'required' => true,
			],
		],
		[
			'label' => 'Số người lớn',
			'name' => 'number_adults',
			'type' => Input::NUMBER,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Số trẻ em',
			'name' => 'number_children',
			'type' => Input::NUMBER,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Ngày bắt đầu',
			'name' => 'date_star',
			'type' => Input::DATE,
			'attributes' => [
				'required' => 'true',
				'class' => 'form-control email-inp-wrap',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Ngày kết thúc',
			'name' => 'date_done',
			'type' => Input::DATE,
			'attributes' => [
				'required' => 'true',
				'class' => 'form-control email-inp-wrap',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Số phòng',
			'name' => 'room_number',
			'type' => Input::NUMBER,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'label' => 'Ghi chú',
			'name' => 'Note',
			'type' => Input::TEXT,
			'attributes' => [
				'required' => true,
				'class' => 'form-control',
				'placeholder' => '',
			],
		],
		[
			'value' => 'Đăng kí',
			'type' => Input::SUBMIT,
			'attributes' => [
				'class' => 'btn btn-primary btn-submit',
				'onclick' => 'clear()',
			],
		],
	],
]);


use VC\Slider\Abstracts\Size;
use VC\Slider\Abstracts\SliderType;
use VC\Slider\Facades\SliderManager;

SliderManager::add([
    'name'    => 'Slider Homepage',
    'type'    => SliderType::SLICK,
    'style'   => 'nf-style-1',
    'size'    => Size::SIZE_3X1,
    'fields'  => [
        [
            'label'       => 'Slider',
            'name'        => 'slider',
            'type'        => Input::GALLERY,
            'description' => 'Gallery with meta field, for now we support text and textarea on meta field.',
        ],
    ],
    'options' => [
        'slidesToShow'   => 1,
        'slidesToScroll' => 1,
        'autoplay'       => true,
        'pauseOnHover'   => true,
        'infinite'       => true,
        'dots'           => false,
        'arrows'		 => true,
    ],
]);