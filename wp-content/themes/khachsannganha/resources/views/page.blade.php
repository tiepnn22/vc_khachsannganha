@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_page,
            'banner_img_check' => $banner_img_check
        ];
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')
    @while(have_posts())

        {!! the_post() !!}

        <section class="page-page">
			<div class="container">
				
		        <div class="single-content">
		            {!! the_content() !!}
		        </div>

			</div>
        </section>

    @endwhile
@endsection