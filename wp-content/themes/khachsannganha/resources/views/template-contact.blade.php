@extends('layouts.full-width')


@section('content')
    @while(have_posts())

        {!! the_post() !!}

        <section class="page-contact">

            <div class="page-contact-content">
                <div class="contact-map">
                    {!! get_field('contact_map') !!}
                </div>
                <div class="contact-info">
                    <div class="container">
                        <div class="row">

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="contact-form">
                                    {!! do_shortcode("[nf_contact_form name='form-contact']") !!}
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="contact-content">
                                    {!! the_content() !!}
                                </div>
                                <div class="contact-socical">
                                    <div class="title-footer">
                                        <h2>
                                            {{ _e('Mạng xã hội', 'nganha') }}
                                        </h2>
                                    </div>
                                    <div class="socical">
                                        <a href="{{ get_option('header_top_socical_facebook') }}"></a>

                                        <a href="tel:{{ get_option('header_top_tel_phone') }}"></a>

                                        <a href="{{ get_option('header_top_socical_youtobe') }}"></a>

                                        <a href="{{ get_option('header_top_socical_tripadviso') }}"></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                 </div>
            </div>
        </section>

    @endwhile
@endsection