<footer class="footer">
	<div class="footer-top">
	    <div class="container">
	    	<div class="footer-top-content">
		    	<div class="row">

			        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 footer-address">
			        	<div class="title-footer">
			        		<h2>{{ _e('Địa chỉ', 'nganha') }}</h2>
			        	</div>

			        	<span>
			        		{!! get_option('footer_address') !!}
			        	</span>
			        </div>

			        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 footer-news">
						@php $id_footer_news = get_option('footer_news'); @endphp

			        	<div class="title-footer">
			        		<h2>{{ get_cat_name($id_footer_news) }}</h2>
			        	</div>

						<ul>
		                    @php
		                        $shortcode = "[listing cat=$id_footer_news layout='partials.sections.content-footer-news' per_page='5']";
		                        echo do_shortcode($shortcode);
		                    @endphp
		                </ul>
			        </div>

			        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 footer-form">
			        	<div class="title-footer">
			        		<h2>{{ _e('Liên hệ với chúng tôi', 'nganha') }}</h2>
			        	</div>
						<a href="{{ get_option('header_top_socical_facebook') }}">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
			        	<a href="tel:{{ get_option('header_top_tel_phone') }}">
			        		<i class="fa fa-phone" aria-hidden="true"></i>
			        	</a>
						{!! do_shortcode("[nf_contact_form name='form-footer']") !!}
			        </div>

				</div>
			</div>
	    </div>
	</div>

	<div class="footer-bottom">
		<div class="container">
			<div class="footer-bottom-content">

				<div class="footer-socical">
					<div class="footer-socical-title">
						<span>{{ _e('Theo dõi chúng tôi :', 'nganha') }}</span>
					</div>

					{{ view('partials.socical') }}
				</div>

				<div class="footer-copyright">
					<span>
						{!! get_option('footer_copyright') !!}
					</span>
				</div>

			</div>
		</div>
	</div>
</footer>

