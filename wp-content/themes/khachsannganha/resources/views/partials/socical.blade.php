<div class="socical">
	<a href="{{ get_option('header_top_socical_facebook') }}">
		<i class="fa fa-facebook" aria-hidden="true"></i>
	</a>
	<a href="{{ get_option('header_top_socical_google') }}">
		<i class="fa fa-google-plus" aria-hidden="true"></i>
	</a>
	<a href="{{ get_option('header_top_socical_twitter') }}">
		<i class="fa fa-twitter" aria-hidden="true"></i>
	</a>
	<a href="{{ get_option('header_top_socical_youtobe') }}">
		<i class="fa fa-youtube-play" aria-hidden="true"></i>
	</a>
	<a href="{{ get_option('header_top_socical_tripadviso') }}">
		<i class="fa fa-tripadvisor" aria-hidden="true"></i>
	</a>
</div>