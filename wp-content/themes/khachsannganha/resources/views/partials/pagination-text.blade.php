<div class="pagination-text">
    <span>
        @php
            $current_page = max(1, get_query_var('paged'));
        @endphp
        {{ "Trang ".$current_page }}
    </span>
</div>