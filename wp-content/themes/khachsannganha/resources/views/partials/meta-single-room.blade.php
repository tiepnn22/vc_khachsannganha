			@php
				$room_bed = get_field('room_bed', $id);
				$room_television = get_field('room_television', $id);
				$room_bath = get_field('room_bath', $id);
				$room_eat = get_field('room_eat', $id);
				$room_wifi = get_field('room_wifi', $id);
			@endphp

			@empty (!$room_bed)
			<div class="meta-info">
				<span><i class="fa fa-bed" aria-hidden="true"></i></span>
				<span>{{ $room_bed }}</span>
			</div>
			@endempty

			@empty (!$room_television)
			<div class="meta-info">
				<span><i class="fa fa-television" aria-hidden="true"></i></span>
				<span>{{ $room_television[0] }}</span>
			</div>
			@endempty

			@empty (!$room_bath)
			<div class="meta-info">
				<span><i class="fa fa-bath" aria-hidden="true"></i></span>
				<span>{{ $room_bath[0] }}</span>
			</div>
			@endempty

			@empty (!$room_eat)
			<div class="meta-info">
				<span><i class="fa fa-cutlery" aria-hidden="true"></i></span>
				<span>{{ $room_eat[0] }}</span>
			</div>
			@endempty

			@empty (!$room_wifi)
			<div class="meta-info">
				<span><i class="fa fa-wifi" aria-hidden="true"></i></span>
				<span>{{ $room_wifi[0] }}</span>
			</div>
			@endempty