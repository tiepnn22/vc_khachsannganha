<article class="item">
	<figure>
		<a href="{{ $url }}">
			<img src="{{ asset2('images/3x3.png') }}" style="background-image: url({{ getPostImage($id, 'news-sidebar') }})" alt="{{ $title }}">
		</a>
	</figure>
	<div class="info">
		<div class="title-news">
			<a href="{{ $url }}">
				<h3>
					{{ $title }}
				</h3>
			</a>
		</div>
		<div class="meta">
			<span class="date">
				<i class="fa fa-calendar" aria-hidden="true"></i>
				{{ $date }}
			</span>
		</div>
		<div class="desc">
            @php
                if (get_the_excerpt() != '') {
                    $excerpt = createExcerptFromContent(get_the_excerpt(), 20);
                } else {
                    $excerpt = '';
                }
            @endphp
            {{ $excerpt }}
		</div>
	</div>
</article>