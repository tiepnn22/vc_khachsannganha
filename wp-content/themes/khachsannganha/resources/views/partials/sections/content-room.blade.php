
<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 item">
	<figure>
		<a href="{{ $url }}">
			<div>
				<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ getPostImage($id, 'room') }})" alt="{{ $title }}">
			</div>
		</a>
	</figure>
	<div class="info">
		<div class="title-room">
			<a href="{{ $url }}">
				<h3>
					{{ $title }}
				</h3>
			</a>
		</div>
		<div class="meta">
			{{ view('partials.meta-single-room') }}
		</div>
	</div>
</article>