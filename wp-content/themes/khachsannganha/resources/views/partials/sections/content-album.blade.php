<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 item">
	<a href="{{ getPostImage($id) }}" class="venobox" title="{{ $title }}" data-gall="gallery-gallery">
		<img src="{{ asset2('images/3x3.png') }}" style="background-image: url({{ getPostImage($id, 'album') }})" alt="{{ $title }}">
	</a>
</article>

