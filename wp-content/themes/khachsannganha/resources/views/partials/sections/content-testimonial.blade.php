<article class="item">
	<div class="testimonial-content">
	    {!! wpautop( $content ) !!}
	</div>

	<div class="title-testimonial">
		<h3>
			{{ $title }}
		</h3>
	</div>
</article>