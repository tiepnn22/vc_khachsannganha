
<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 item">
	<figure>
		<a href="{{ $url }}">
			<div>
				<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ getPostImage($id, 'room') }})" alt="{{ $title }}">
			</div>
		</a>
	</figure>
	<div class="info">
		<div class="title-room">
			<a href="{{ $url }}">
				<h3>
					{{ $title }}
				</h3>
			</a>
		</div>
		<div class="meta">
			{{ get_field('room_price', $id) }}
		</div>
		<a href="{{ $url }}" class="read-more">
            {{ _e('Xem thêm', 'nganha') }} <i class="fa fa-angle-double-right" aria-hidden="true"></i>
		</a>
	</div>
</article>