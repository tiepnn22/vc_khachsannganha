<section class="banner-no-home">

    @if (!empty($banner_img_check))
        <img src="{{ asset2('images/9x1.png') }}" style="background-image: url({{ $banner_img_check }})">
    @else
        <img src="{{ asset2('images/9x1.png') }}" style="background-image: url({{ get_option('banner_no_home') }})">
    @endif

    <div class="page-title">
        <h2>
            {{ $page_title }}
        </h2>
    </div>

</section>