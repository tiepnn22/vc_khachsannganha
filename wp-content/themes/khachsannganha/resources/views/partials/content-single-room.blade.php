<section class="page-single-room">
    <div class="container">
        <div class="row">

            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-room-content">
				<div class="meta">
					<div class="meta-info">
						<span>{{ _e('Số lượng', 'nganha') }} : {{ $room_number }}</span>
					</div>
					<div class="meta-info">
						<span>{{ _e('Diện tích', 'nganha') }} : {{ $room_acreage }}</span>
					</div>
					<div class="meta-info">
						<span>{{ _e('Đơn giá', 'nganha') }} : {{ $room_price }}</span>
					</div>
				</div>

				<div class="room-gallery">
					<div class="slider slider-for">
						<figure>
							<img src="{{ asset2('images/2x1.png') }}" style="background-image: url({{ getPostImage(get_the_ID()) }})" alt="{{ the_title() }}">
						</figure>
						@foreach ( $room_gallery as $room_gallery_kq )
							<figure>
								<img src="{{ asset2('images/2x1.png') }}" style="background-image: url({{ $room_gallery_kq['url'] }})" alt="{{ $room_gallery_kq['title'] }}">
							</figure>
						@endforeach
					</div>
					<div class="slider slider-nav">
						<figure>
							<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ getPostImage(get_the_ID()) }})" alt="{{ the_title() }}">
						</figure>
						@foreach ( $room_gallery as $room_gallery_kq )
						    <figure>
						    	<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ $room_gallery_kq['url'] }})" alt="{{ $room_gallery_kq['title'] }}">
						    </figure>
						@endforeach
					</div>
				</div>

				@if(!empty($room_standard))
					<div class="room-standard">
			        	<div class="title-footer">
			        		<h2>{{ _e('Phòng tiêu chuẩn', 'nganha') }}</h2>
			        	</div>
						<div class="room-standard-content">
							<ul>
								@foreach ( $room_standard as $room_standard_kq )
									<li>
									    {{ $room_standard_kq['room_standard_info'] }}
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				@endif

				<div class="room-content">
					{!! wpautop(the_content()) !!}
				</div>
            </div>

		    <aside class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 sidebar">
		    	<h2 class="sidebar-title">{{ _e('Đăng kí đặt phòng', 'nganha') }}</h2>
				{!! do_shortcode("[nf_contact_form name='form-sidebar']") !!}
		    </aside>

        </div>
    </div>
</section>
