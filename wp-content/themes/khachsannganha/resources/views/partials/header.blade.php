<header class="header">
	<div class="header-top">
		<div class="container">
			<div class="header-top-content">

				<div class="header-hotline">
					<span>
						{{ _e('Hotline:', 'nganha') }}
						<a href="tel:{{ get_option('header_top_tel_phone') }}">
							{{ get_option('header_top_tel') }}
						</a>
					</span>
				</div>
				<div class="header-socical">

					{{ view('partials.socical') }}

				</div>

			</div>
		</div>
	</div>

	<div class="header-bottom">
		<div class="header-info">
			<div class="container">
				<div class="header-bottom-content">

					<div class="logo">
						<a href="{{ get_option('home') }}">
							<img src="{{ get_option('header_bottom_logo') }}" alt="{{ get_option('blogname') }}">
						</a>
					</div>

					<div class="slogan">
						<span>{{ get_option('header_bottom_slogan_small') }}</span>
						<span>{{ get_option('header_bottom_slogan_large') }}</span>
					</div>

		            <div class="image-right">
		            	<img src="{{ get_option('header_bottom_image_right') }}" alt="">
		            </div>

		        </div>
			</div>
		</div>

	    <nav class="menu">
	    	<div class="container">

		        <div class="main-menu">
		            @if (has_nav_menu('main-menu'))
		                {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']) !!}
		            @endif
		        </div>
		        <div class="mobile-menu"></div>

	            <div class="search-box">
	                <form action="{!! esc_url( home_url( '/' ) ) !!}">
	                    <input type="text" placeholder="Tìm kiếm" id="search-box" name="s" value="{!! get_search_query() !!}">
	                    <div class="search-icon">
	                        <i class="fa fa-search" aria-hidden="true"></i>
	                    </div>
	                </form>
	            </div>

		    </div>
	    </nav>
	</div>
</header>