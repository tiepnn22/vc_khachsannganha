<section class="page-single-news">
    <div class="container">
        <div class="single-news-content">

            <h1 class="entry-title">{{ the_title() }}</h1>

            <div class="meta">
              <span class="date">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                {{ get_the_date() }}
              </span>
            </div>

            {{ view('partials.social-bar') }}

            <div class="single-content">
                {!! the_content() !!}
            </div>

        </div>
    </div>
</section>


<section class="related-news">
    <div class="container">
        <div class="page-category-content related-news-content">
            <div class="related-title">
                <h2>
                  {{ _e('Bài viết liên quan', 'tamlan') }}
                </h2>
            </div>

            <div class="row">
                @php
                    $shortcode = "[listing post_type='post' cat=$id_category excludes='".get_the_ID()."' layout='partials.sections.content-related-post' per_page='3']";
                    echo do_shortcode($shortcode);
                @endphp
            </div>

        </div>
    </div>
</section>
