
<h2 class="title-widget">
	{{get_cat_name($id_category)}}
</h2>

@php
    $shortcode = "[listing cat=$id_category layout='partials.sections.content-listpostcategory' per_page=$number_post]";
    echo do_shortcode($shortcode);
@endphp
