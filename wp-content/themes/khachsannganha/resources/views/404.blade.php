@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_page,
            'banner_img_check' => $banner_img_check
        ];
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')

	<section class="page-content">
		<div class="container">
			<div class="page-404">
		        <span class="page-404-text-err">
		        	404
		        </span>
		        <h1 class="page-404-title">
		        	<?php _e('Chúng tôi xin lỗi...', 'nganha');?>
		        </h1>
		        <p>
		        	<?php _e('Không tìm thấy trang.', 'nganha');?>
		        </p>
		        <p>
		        	<a class="page-404-title2" href="<?php echo get_option('home'); ?>">
		        		<?php _e('Trở về trang chủ', 'nganha');?>
		        	</a>
		        </p>
			</div>
		</div>
	</section>

@endsection