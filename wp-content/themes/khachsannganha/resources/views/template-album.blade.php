@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_page,
            'banner_img_check' => $banner_img_check
        ];
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')

    <section class="page-album">
        <div class="container">

            <div class="page-album-content">
                <div class="row">
                    @php
                        $shortcode = "[listing post_type='album' layout='partials.sections.content-album' paged='yes' per_page='12']";
                        echo do_shortcode($shortcode);
                    @endphp
                    @include('partials.pagination-text')
                </div>
            </div>

        </div>
    </section>

@endsection
