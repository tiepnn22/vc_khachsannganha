@extends('layouts.full-width')

@section('content')

	<?php
		echo do_shortcode("[vc-slider name='Slider Homepage']");
	?>

	@empty (!$home_room_type)
		<section class="home-room-type">
			<div class="container">
				<div class="page-room-content home-room-type-content">

					<div class="title-section">
						<h2>{{ $home_room_type_title }}</h2>
					</div>

		            <div class="row">
						@foreach ( $home_room_type as $home_room_type_kq )
						    @php
						        $data = [
						            'id' => $home_room_type_kq,
						            'title' => get_the_title($home_room_type_kq),
						            'url' => get_permalink($home_room_type_kq)
						        ];
						    @endphp
						    {!!  view('partials.sections.content-home-room-type', $data)  !!}
					    @endforeach
					</div>

				</div>
			</div>
		</section>
	@endempty

	@empty (!$home_introduction)
		<section class="home-introduction" style="background-image: url({{ $home_introduction_bg }});">
			<div class="container">
				<div class="home-introduction-content">
					<div class="home-introduction-info wow fadeInLeft" data-wow-delay="0.4s">
						{!! $home_introduction !!}
					</div>
				</div>
			</div>
		</section>
	@endempty

	@empty (!$home_video)
		<section class="home-video">
			<div class="container">
				<div class="home-video-content">
		            <iframe width="420" height="315" src="{{ str_replace('watch?v=','embed/',strip_tags($home_video)) }}" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</section>
	@endempty

	<section class="home-testimonial">
		<div class="container">
			<div class="home-testimonial-content">

			<div class="title-section">
				<h2>{{ _e('Testimonials', 'nganha') }}</h2>
			</div>
			    @php
			        $shortcode = '[listing post_type="testimonial" layout="partials.sections.content-testimonial" per_page="-1"]';
			        echo do_shortcode($shortcode);
			    @endphp
			</div>

		</div>
	</section>

@endsection