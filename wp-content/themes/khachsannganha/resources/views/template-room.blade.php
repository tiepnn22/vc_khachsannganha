@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_page,
            'banner_img_check' => $banner_img_check
        ];
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')

    <section class="page-room">
        <div class="container">

            <div class="page-room-content">
                <div class="row">
                    @php
                        $shortcode = "[listing post_type='room' layout='partials.sections.content-room' paged='yes' per_page='6']";
                        echo do_shortcode($shortcode);
                    @endphp
                </div>
            </div>

        </div>
    </section>

@endsection
