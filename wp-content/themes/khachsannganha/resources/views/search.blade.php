@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_page,
            'banner_img_check' => $banner_img_check
        ];
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')

    @if (!have_posts())
        <div class="alert alert-warning">
            {{ __('Xin lỗi, không tìm thấy kết quả.', 'nganha') }}
        </div>
        {!! get_search_form(false) !!}
    @endif

    <section class="page-search">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-category-content page-search-content">
                    @while(have_posts())

                        {!! the_post() !!}

                            @php
                                $data = [
                                    'id' => get_the_ID(),
                                    'title' => get_the_title(),
                                    'url' => get_permalink(),
                                    'date' => get_the_date()
                                ];
                            @endphp
                            {!!  view('partials.sections.content-search', $data)  !!}

                    @endwhile
                    <div class="page-navi">
                        <div class="container">
                            @php
                                global $wp_query;

                                $big = 999999999;

                                echo paginate_links(array(
                                	'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                                	'format' => '?paged=%#%',
                                	'current' => max(1, get_query_var('paged')),
                                	'total' => $wp_query->max_num_pages,
                                	'prev_text' => __('«'),
                                	'next_text' => __('»'),
                                ));
                            @endphp
                        </div>
                    </div>
                </div>
                <aside class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 page-category-content sidebar">
                    <?php dynamic_sidebar('sidebar-category');?>
                </aside>
            </div>
        </div>
    </section>


@endsection

