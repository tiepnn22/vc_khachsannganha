@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_category,
            'banner_img_check' => $banner_img_check
        ];
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')

    @while(have_posts())
	
		{!! the_post() !!}

        @include('partials.content-single-' . get_post_type())

    @endwhile

@endsection
