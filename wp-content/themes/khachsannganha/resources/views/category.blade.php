@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_category,
            'banner_img_check' => $banner_img_check
        ];
        global $wp_query;
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')

    <section class="page-category">
        <div class="container">
            <div class="row">

                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-category-content">
                    @php
                        $shortcode = "[listing cat=$id_category layout='partials.sections.content-category' paged='yes' per_page='5']";
                        echo do_shortcode($shortcode);
                    @endphp
                    @include('partials.pagination-text')
                </div>
                <?php get_sidebar();?>

            </div>
        </div>
    </section>

@endsection