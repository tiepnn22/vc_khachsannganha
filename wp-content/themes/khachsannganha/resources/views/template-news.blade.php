@extends('layouts.full-width')

@section('banner')

    @php
        $data = [
            'page_title' => $name_page,
            'banner_img_check' => $banner_img_check
        ];
    @endphp
    {!!  view('partials.sections.banner-no-home', $data)  !!}

@endsection

@section('content')

    <section class="page-news">
        <div class="container">
            <div class="row">

                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-category-content page-news-content">
                    @php
                        $shortcode = "[listing cat=$category_page_news layout='partials.sections.content-category' paged='yes' per_page='5']";
                        echo do_shortcode($shortcode);
                    @endphp
                    @include('partials.pagination-text')
                </div>

                <aside class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 page-category-content sidebar">
                    <?php dynamic_sidebar('sidebar-category');?>
                </aside>

            </div>
        </div>
    </section>

@endsection
