$(document).ready(function () {
       $('.slider-banner').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: true,
              autoplay: true,
              fade: false,
              pauseOnHover: true
       });
});