import 'jquery';
import 'bootstrap';
import slick from 'slick-carousel/slick/slick.min.js';
import meanmenu from 'jquery.meanmenu/jquery.meanmenu.min.js';
import venobox from 'venobox/venobox/venobox.min.js';
import WOW from 'wow/dist/wow.min.js';
import banner from './banner/banner';

$(document).ready(function() {
    $('body').click(function(event){
        $(this).find('.search-box form input').fadeOut();
    });
	$('.search-box form .search-icon').click(function(event){
		$(this).parent().find('input').fadeToggle();
        event.stopPropagation();//ko tính click body
	});
    $('.search-box form input').click(function(event){
        $(this).fadeIn(); 
        event.stopPropagation();
    });
    //meanmenu
    $('.main-menu').meanmenu({
        meanScreenWidth: "1024",
        meanMenuContainer: ".mobile-menu",
    });
    $('.home-testimonial .msc-listing').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        // autoplay: true,
        fade: true,
        pauseOnHover: true
    });

    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav',
    });
    $('.slider-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      centerMode: true,
      focusOnSelect: true,
      autoplay: false,
      // autoplaySpeed: 2000,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            arrows: false,
            centerPadding: '10px',
            slidesToShow: 4
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerPadding: '10px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerPadding: '10px',
            slidesToShow: 2
          }
        }
      ]
    });
    $('.venobox').venobox();
    new WOW().init();

    if($('.page-numbers').length){
        $('.page-category-content').find('.pagination-text').css('display','block');

    }
    else{
        $('.page-category-content').find('.pagination-text').css('display','none');
         $('.page-category-content').find('.pagination').css('padding-bottom','20px');
    }
    //display pagination in album
    if($('.page-numbers').length){
        $('.page-album-content').find('.pagination-text').css('display','block');

    }
    else{
        $('.page-album-content').find('.pagination-text').css('display','none');
         $('.page-album-content').find('.pagination').css('padding-bottom','20px');
    }
});