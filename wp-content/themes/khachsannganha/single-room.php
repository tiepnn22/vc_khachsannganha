<?php

	$name_category = get_the_title();
	$room_id = get_the_ID();

	$room_number = get_field('room_number', $room_id);
	$room_acreage = get_field('room_acreage', $room_id);
	$room_price = get_field('room_price', $room_id);
	$room_gallery = get_field('room_gallery', $room_id);
	$room_standard = get_field('room_standard', $room_id);

	$data = [
	    'name_category' => $name_category,
	    'room_id' => $room_id,
	    'room_number' => $room_number,
	    'room_acreage' => $room_acreage,
	    'room_price' => $room_price,
	    'room_gallery' => $room_gallery,
	    'room_standard' => $room_standard,
	];

	view('single', $data);

?>
