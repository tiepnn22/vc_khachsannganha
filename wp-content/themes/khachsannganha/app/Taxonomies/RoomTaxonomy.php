<?php
namespace App\Taxonomies;

use MSC\Tax;

class RoomTaxonomy extends Tax
{
    public function __construct()
    {
        $config = [
            'slug'   => 'room-category',
            'single' => 'Room-Category',
            'plural' => 'Room-Categories'
        ];

        $postType = 'room';

        $args = [];

        parent::__construct($config, $postType, $args);
    }
}
