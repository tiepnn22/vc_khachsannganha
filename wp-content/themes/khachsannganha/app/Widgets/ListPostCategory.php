<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class ListPostCategory extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('listpostcategory', 'nganha'),
            'label'       => __('Widget show post category', 'nganha'),
            'description' => __('Widget show post category', 'nganha'),
        ];

        $fields = [
            [
                'label' => __('Select category', 'nganha'),
                'name'  => 'id_category',
                'type'  => 'select',
                'options' => $this->getCategories(),
            ],
            [
                'label' => __('Number show post', 'nganha'),
                'name'  => 'number_post',
                'type'  => 'number',
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function getCategories()
    {
        $categories = getCategories('category');

        $cates[0] = __('Select Category', 'nganha');

        foreach ($categories as $key => $value) {
            $cates[$key] = $value;
        }

        return $cates;
    }  

    public function handle($instance)
    {
        $data = [
            'id_category' => $instance['id_category'],
            'number_post' => $instance['number_post'],
        ];

        view('partials.widgets.widget-listpostcategory', $data);
                            
    }
}

