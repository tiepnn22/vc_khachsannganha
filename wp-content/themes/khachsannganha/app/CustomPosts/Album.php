<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Album extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'album';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'Album';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'Albums';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-images-alt2'];

}
