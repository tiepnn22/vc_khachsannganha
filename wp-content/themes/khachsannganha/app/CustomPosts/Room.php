<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Room extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'room';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'Room';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'Room';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-admin-home'];

}
