<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Testimonial extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'testimonial';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'Testimonial';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'Testimonials';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-admin-users'];

}
